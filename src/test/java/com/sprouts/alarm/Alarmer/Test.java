package com.sprouts.alarm.Alarmer;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sprouts.spm_framework.sql.SQLService;


public class Test {
    public static void main(String[] args) throws SQLException {
        SQLService sqlService = new SQLService();
        sqlService.connectDatabase();
        ResultSet set = sqlService.attachList("select * from list_spm_web;");
        while (set.next()) {
            System.out.println(set.getString("monitorname"));
        }
    }
}
